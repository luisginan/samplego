# README #

# Cara panggil API :
1. Methode GET : http://localhost:8080/pet?id=1
2. Methode POST: http://localhost:8080/pet
   Sisipkan json dalam body : 
   
```
#!json

   {
     "id" : 1,
     "name" : "Doggy",
     "age" : 5,
     "photo" : "http://doggy.photos.com/main.jpg"
   }
```

3. Method PUT : http://localhost:8080/pet?id=1
   Sisipkan json dalam body : 
   
```
#!json

 {
     "id" : 1,
     "name" : "Gukguk",
     "age" : 5,
     "photo" : "http://doggy.photos.com/main.jpg"
   }
```

4. Method DELETE : http://localhost:8080/pet?id=1