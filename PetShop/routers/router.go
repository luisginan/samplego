package routers

import (
	"PetShop/controllers"

	"github.com/astaxie/beego"
)

func init() {
	beego.Router("/", &controllers.MainController{})
	beego.Router("/Pet", &controllers.PetController{})
}
