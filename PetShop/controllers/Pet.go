package controllers

import (
	"encoding/json"

	"github.com/astaxie/beego"
)

type Pet struct {
	Id    int16  `json:"id"`
	Name  string `json:"name"`
	Age   int16  `json:"age"`
	Photo string `json:"photo"`
}

var gPets [1000]Pet

type PetController struct {
	beego.Controller
}

func (c *PetController) Get() {

	var id, _ = c.GetInt16("id")

	for i := 0; i < len(gPets); i++ {
		if gPets[i].Id == id {
			c.Data["json"] = gPets[i]
			break
		}
	}

	c.ServeJSON()
}

func (this *PetController) Post() {
	var ob Pet
	json.Unmarshal(this.Ctx.Input.RequestBody, &ob)

	var isExist = false
	for i := 0; i < len(gPets); i++ {
		if gPets[i].Id == ob.Id {
			isExist = true
			break
		}
	}

	if isExist {
		this.Data["json"] = "Data exist"
	} else {

		var result = false
		for i := 0; i < len(gPets); i++ {
			if gPets[i].Id == 0 {
				gPets[i] = ob
				result = true
				break
			}
		}

		if result == true {
			this.Data["json"] = "Inserted Success"
		} else {
			this.Data["json"] = "Data Full"
		}
	}
	this.ServeJSON()
}

func (this *PetController) Put() {

	var id, _ = this.GetInt16("id")

	var pet Pet
	json.Unmarshal(this.Ctx.Input.RequestBody, &pet)

	var result = false
	for i := 0; i < len(gPets); i++ {
		if gPets[i].Id == id {
			gPets[i] = pet
			result = true
			break
		}
	}

	if result == true {
		this.Data["json"] = "Updated success"
	} else {
		this.Data["json"] = "Not found"
	}

	this.ServeJSON()

}

func (this *PetController) Delete() {

	var id, _ = this.GetInt16("id")

	var result = false
	for i := 0; i < len(gPets); i++ {
		if gPets[i].Id == id {
			gPets[i].Id = 0
			gPets[i].Name = ""
			gPets[i].Age = 0
			gPets[i].Photo = ""
			result = true
		}
	}

	if result == true {
		this.Data["json"] = "Delete Success"
	} else {
		this.Data["json"] = "Not found"
	}

	this.ServeJSON()
}
